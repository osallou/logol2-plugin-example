module gitlab.inria.fr/osallou/logol2-plugin-example

go 1.13

require (
	gitlab.inria.fr/osallou/logol2-lib v0.5.0
	go.uber.org/zap v1.13.0
)
