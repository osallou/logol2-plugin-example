package logolpluginexample

import (
	"strconv"
	"strings"

	"go.uber.org/zap"

	logol "gitlab.inria.fr/osallou/logol2-lib"
)

var logger = logol.GetLogger("logol.pluginexample")

// FindPluginExampleComponent is a plugin example
type FindPluginExampleComponent struct {
	logol.FindExactComponent
	params []string
}

// NewSearchComponent must return an instance of a SearchComponent
// Called when grammar variable Plugin/Search is set
// Must set a chan of Match in cm variable
func NewSearchComponent(ctx logol.Context, modelName string, varName string, hasPrevMatch bool, prevMatch logol.Match, params string) logol.SearchComponent {
	fc := FindPluginExampleComponent{}
	fc.Position = ctx.Position
	fc.ModelName = modelName
	fc.VarName = varName
	fc.HasPrevMatch = hasPrevMatch
	fc.PrevMatch = prevMatch
	fc.Cm = make(chan logol.Match)
	if ctx.Grammar != nil {
		fc.Component = ctx.Grammar.Models[modelName].Vars[varName]
	}
	fc.Ctx = ctx
	fc.params = strings.Split(params, ",")
	return fc
}

// Check checks a match (is position ok)
func (fc FindPluginExampleComponent) Check(match logol.Match) bool {
	return true
}

// PostControl evaluates a match to determine if it fills conditions
// Called when grammar variable Plugin/Check is set
// This plugins checks there is at least X% of "c" in match
// X percentage is defined in params
func PostControl(ctx logol.Context, match logol.Match, params string) bool {
	pluginParams := strings.Split(params, ",")
	if len(pluginParams) == 0 {
		logger.Error("missing percentage in params", zap.String("plugin", "pluginexample"))
		return false
	}

	percent, percentErr := strconv.Atoi(pluginParams[0])
	if percentErr != nil {
		logger.Error("Failed to evaluate percentage")
		return false
	}

	percentC := Percent(ctx, match, 'c')
	//countC := Count(ctx, match, 'c')

	//if ((countC * 100) / int(match.Len)) < percent {
	if percentC < percent {
		return false
	}
	return true
}

// Items search for matches, must send results to chan
func (fc FindPluginExampleComponent) Items() {

	logger.Debug("FindPluginExampleComponent:Items", zap.String("model", fc.ModelName), zap.String("var", fc.VarName))
	if fc.HasPrevMatch && fc.PrevMatch.Defined {
		logger.Debug("FindPluginExampleComponent: Already found, skipping", zap.Any("match", fc.PrevMatch))
		if fc.Check(fc.PrevMatch) {
			fc.Cm <- fc.PrevMatch
		}
		close(fc.Cm)
		return
	}

	/* If could not determine item, returns a Defined = false match
	match := logol.Match{
		Model:    fc.ModelName,
		VarName:  fc.VarName,
		Position: fc.Ctx.Position,
		Pattern:  fc.Component.Comment,
		Defined:  false,
	}
	fc.Cm <- match
	*/

	// Always close channel once search is over
	close(fc.Cm)
}

// Count returns the number of character occurence
func Count(ctx logol.Context, match logol.Match, c rune) int {
	content := ctx.SequenceLru.GetContent(int(match.Position), int(match.Position+match.Len))
	logger.Debug("plugin", zap.String("content", content), zap.Any("match", match))

	if len(content) == 0 {
		logger.Error("plugin: no content", zap.Any("match", match))
		return 0
	}
	return countOccurence(content, c)
}

// Percent computes percentage of character occurence
func Percent(ctx logol.Context, match logol.Match, c rune) int {
	count := Count(ctx, match, c)
	percent := count * 100 / int(match.Len)
	return percent
}

func countOccurence(content string, c rune) int {
	count := 0
	for _, char := range content {
		if char == c {
			count++
		}
	}
	return count
}
