package logolpluginexample

import (
	"testing"

	logol "gitlab.inria.fr/osallou/logol2-lib"
)

var ctx logol.Context

func setup() {
	if ctx.SequenceLru == nil {
		seq := logol.NewSequence("sequence.txt")
		lru := logol.NewSequenceLru(seq)
		ctx = logol.Context{
			Position: 0,
			Spacer:   false,
			// Match: nil,
			Progress:      make(chan string),
			Grammar:       &logol.Grammar{},
			SequenceInput: "sequence.txt",
			Sequence:      &seq,
			SequenceLru:   &lru,
		}
	}
}

func TestCount(t *testing.T) {
	setup()
	match := logol.Match{
		Position: 0,
		Len:      4,
	}
	nbC := Count(ctx, match, 'c')
	if nbC != 4 {
		t.Errorf("Expecting to find 4 C, got %d", nbC)
	}

	match = logol.Match{
		Position: 2,
		Len:      4,
	}
	nbC = Count(ctx, match, 'c')
	if nbC != 2 {
		t.Errorf("Expecting to find 2 C, got %d", nbC)
	}
}

func TestPercent(t *testing.T) {
	setup()
	match := logol.Match{
		Position: 0,
		Len:      4,
	}
	percentC := Percent(ctx, match, 'c')
	if percentC != 100 {
		t.Errorf("Expecting to find 100 percent C, got %d", percentC)
	}

	match = logol.Match{
		Position: 2,
		Len:      4,
	}
	percentC = Percent(ctx, match, 'c')
	if percentC < 50 {
		t.Errorf("Expecting to find 50 percent C, got %d", percentC)
	}
}
